<?php 

error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once "include/cards.php";

$card_url = "http://".$_SERVER["HTTP_HOST"]."/e24/cards/";

$card = new CardSuite($card_url);

# uncomment to see names of all available suites 
# echo "<p>".$card->getSuites();

# uncomment to get a specific suite of cards as json
# echo "<p>".$card->getSuite("scifi");

# uncomment to test no suite given, fallback on default value
# echo "<p>".$card->getSuite();

# For some reason FallbackResource /index.php did not work, so access
# to the card suite will thru ajax call to GET requests with params

# FallbackResource... 

#$uri = ltrim($_SERVER['REQUEST_URI'], '/');   // Trim leading slash(es)

# example: list all suites
#if ($uri == "e24/suite/all") {
#    echo "<p>".$card->getSuites();
#}

# example: specific suite
#if ($uri == "e24/suite/scifi") {
#    echo "<p>".$card->getSuite("scifi");
#}

# example: default suite
#if ($uri == "e24/suite") {
#    echo "<p>".$card->getSuite();
#}

#phpinfo();

if (isset($_GET["suite"])) {
    echo $card->getSuite($_GET["suite"]);
} else {
    require_once "include/index.html";
}
?>