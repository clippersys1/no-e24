# memory card game
## kristoffer hell (info@clippersys.eu)

### `backend:` php server which scans a directory of for images and exports as json. Preference would have been to make .htaccess file work as REST service using FallbackResource, local serve (xubuntu) for some reason did not support, so went with  GET request

index.php and include card.php

PHP is OO.

### `frontend:` html, css, jquery. The Single Source of Truth for the game board is the DOM wrapped in the `#board` element and children.

### `.card` is for CSS, `.inplay` is used by the javascript to inform which cards have not yet been taken off the board.

### Had time allowed I would hade liked to have finished the AI functionality, to make the computer remember the cards that have been shown.

