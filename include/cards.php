<?php

class CardSuite {

    private $suites = ["scifi","aquatic"];
    private $json = [];
    private $card_url = "";

    # the constructor
    public function __construct($url) {
        $this->card_url = $url;
    }

    # core funstionality, reads the given directory for images 
    private function getCards($path) {
        $imgs = scandir($path);
        return $imgs;
    }

    private function isSuite($suite) {
        foreach ($this->suites as $tmp) {
            if ($tmp == $suite) return true;
        }
        return false;
    }

    # give suite a default value in case called with none
    public function getSuite($suite = "scifi") {
        $path = "cards/".$suite;
        if (! $this->isSuite($suite)) {
            return "ERROR, no such suite: ".$suite ;
        }
        $tmp = $this->getCards($path);
        $cards = [];
        foreach ($tmp as $card) {
            $arr = explode(".",$card);
            # only keep the images
            if ($arr[count($arr)-1] == "jpg") {
                array_push($cards,$this->card_url.$suite."/".$card);
            }
        }
        return json_encode(array_merge($cards,$cards));
    }

    # utility function, currently not used
    public function getSuites() {
        return json_encode($this->suites);
    }

    # utility function, currently not used
    public function getCardUrl() {
        return $this->card_url;
    }

}

?>